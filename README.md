# Tke-cluster-controller

[![Alauda coverage](https://img.shields.io/badge/coverage-44.6%25-yellowgreen.svg)](https://bitbucket.org/mathildetech/tke-cluster-controller/overview)

----

Tke-cluster-controller is a kubernetes controller. It use kubebuiler(1.0.8) to generate the controller framework.
It watch the tke cluster resource, and transform it to official cluster registry cluster resource.

