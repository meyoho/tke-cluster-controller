/*
Copyright 2019 The Alauda Infrastructure Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cluster

import (
	"testing"
	"time"

	clusterregistryv1alpha1 "bitbucket.org/tke-cluster-controller/pkg/apis/clusterregistry/v1alpha1"
	platformv1 "bitbucket.org/tke-cluster-controller/pkg/apis/platform/v1"
	"github.com/onsi/gomega"
	"golang.org/x/net/context"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var c client.Client

var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "tkeclusterregistry"}}

// crcKey = clusterregistryresource key
var crrKey = types.NamespacedName{Name: "tkeclusterregistry", Namespace: "alauda-system"}
var secretKey = types.NamespacedName{Name: "tke-cluster-tkeclusterregistry", Namespace: "alauda-system"}
var tkeKey = types.NamespacedName{Name: "tkeclusterregistry"}
var tekcredentialKey = types.NamespacedName{Name: "lala"}

const timeout = time.Second * 5

func TestReconcile(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	token := "aaabbb"
	instance := &platformv1.Cluster{
		ObjectMeta: metav1.ObjectMeta{
			Name: "tkeclusterregistry",
		},
		Spec: platformv1.ClusterSpec{
			ClusterCIDR: "192.168.0.0/16",
			Machines:    []platformv1.ClusterMachine{},
		},
	}

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()
	// Create test ns: alauda-system
	namespace, err := func() (*corev1.Namespace, error) {
		namespace := &corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{Name: "alauda-system"},
		}
		err := c.Create(context.TODO(), namespace)
		return namespace, err
	}()
	defer c.Delete(context.TODO(), namespace)
	if err != nil {
		t.Errorf("create alauda-system failed. error: %s", err.Error())
	}

	credential := &platformv1.ClusterCredential{
		ObjectMeta: metav1.ObjectMeta{
			Name: "lala",
		},
		ClusterName: "tkeclusterregistry",
		Token:       &token,
	}

	err = c.Create(context.TODO(), credential)
	defer c.Delete(context.TODO(), credential)
	if err != nil {
		t.Errorf("create credential failed. error: %s", err.Error())
	}

	credential1 := &platformv1.ClusterCredential{
		ObjectMeta: metav1.ObjectMeta{
			Name: "lala1",
		},
		ClusterName: "tkeclusterregistry1",
		Token:       &token,
	}
	c.Create(context.TODO(), credential1)
	defer c.Delete(context.TODO(), credential1)

	// Create the Cluster object and expect the Reconcile and Deployment to be created
	err = c.Create(context.TODO(), instance)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(gomega.HaveOccurred())
	defer c.Delete(context.TODO(), instance)
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

	// retriger the event. make sure the status have basic element.
	instance.Status = platformv1.ClusterStatus{
		Conditions: []platformv1.ClusterCondition{
			platformv1.ClusterCondition{
				LastProbeTime:      metav1.Now(),
				LastTransitionTime: metav1.Now(),
				Type:               "HealthCheck",
				Status:             platformv1.ConditionTrue,
			},
		},
		Addresses: []platformv1.ClusterAddress{
			platformv1.ClusterAddress{
				Type: platformv1.AddressAdvertise,
				Host: "10.96.1.1",
				Port: int32(123),
			},
			platformv1.ClusterAddress{
				Type: platformv1.AddressInternal,
				Host: "127.0.0.1",
				Port: int32(234),
			},
			platformv1.ClusterAddress{
				Type: platformv1.AddressPublic,
				Host: "kubernetes",
				Port: int32(443),
			},
		},
	}
	err = c.Status().Update(context.Background(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))
	if err != nil {
		t.Errorf("Update instance fail. %s. error: %s", instance.Name, err.Error())
	}

	clusterRegistryCluster := &clusterregistryv1alpha1.Cluster{}
	secret := &corev1.Secret{}
	g.Eventually(func() error { return c.Get(context.TODO(), secretKey, secret) }, timeout).Should(gomega.Succeed())
	g.Eventually(func() error { return c.Get(context.TODO(), crrKey, clusterRegistryCluster) }, timeout).Should(gomega.Succeed())

	newToken := "bacdef"
	credential.Token = &newToken
	err = c.Update(context.TODO(), credential)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	g.Eventually(func() error { return c.Update(context.TODO(), credential) }, timeout).Should(gomega.Succeed())

	instance.Spec.ClusterCIDR = "10.96.0.1/16"
	err = c.Update(context.TODO(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())
	// TODO
	// g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

	newSecret := &corev1.Secret{}
	time.Sleep(time.Second * 10)
	err = c.Get(context.TODO(), secretKey, newSecret)
	if err != nil {
		t.Errorf("get new secret fail. error: %s", err.Error())
	}
	newCrrInstance := &clusterregistryv1alpha1.Cluster{}
	err = c.Get(context.TODO(), crrKey, newCrrInstance)
	if err != nil {
		t.Errorf("get newCrrInstance fail. error: %s", err.Error())
	}

	newTkeCredential := &platformv1.ClusterCredential{}
	err = c.Get(context.TODO(), tekcredentialKey, newTkeCredential)
	if err != nil {
		t.Errorf("get newTkeInstance fail. error: %s", err.Error())
	}

	// TODO
	// if string(newSecret.Data["token"]) != *newTkeCredential.Token {
	// 	t.Errorf("get token is not equal in tke. secret: %s, tke: %s", string(newSecret.Data["token"]), *newTkeCredential.Token)
	// }

	newTkeInstance := &platformv1.Cluster{}
	err = c.Get(context.TODO(), tkeKey, newTkeInstance)

	// TODO
	// if newCrrInstance.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ClientCIDR != newTkeInstance.Spec.ClusterCIDR {
	// 	t.Errorf("get cidr is not equal in tke. cidr: %s, tke: %s", newCrrInstance.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ClientCIDR, newTkeInstance.Spec.ClusterCIDR)
	// }

	// Delete the tke-cluster and expect Reconcile to be called for Deployment deletion
	g.Expect(c.Delete(context.TODO(), instance)).NotTo(gomega.HaveOccurred())
	g.Eventually(func() error { return c.Delete(context.TODO(), clusterRegistryCluster) }, timeout).
		Should(gomega.MatchError("clusters.clusterregistry.k8s.io \"tkeclusterregistry\" not found"))
	// Manually delete Deployment since GC isn't enabled in the test control plane
	g.Eventually(func() error { return c.Delete(context.TODO(), secret) }, timeout).
		Should(gomega.MatchError("secrets \"tke-cluster-tkeclusterregistry\" not found"))

}

func TestIsClusterHealth(t *testing.T) {
	clusters := []*platformv1.Cluster{
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "health"},
			Status: platformv1.ClusterStatus{
				Conditions: []platformv1.ClusterCondition{
					platformv1.ClusterCondition{
						Type:   "HealthCheck",
						Status: "True",
					},
				},
			},
		},
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "nothealth"},
			Status: platformv1.ClusterStatus{
				Conditions: []platformv1.ClusterCondition{
					platformv1.ClusterCondition{
						Type:   "HealthCheck",
						Status: "False",
					},
				},
			},
		},
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "notexist"},
			Status: platformv1.ClusterStatus{
				Conditions: []platformv1.ClusterCondition{},
			},
		},
	}
	for _, cluster := range clusters {
		clusterHealth := isClusterHealth(cluster)
		if cluster.Name == "notexist" && clusterHealth == true {
			t.Errorf("notexist cluster should ignore. clustername: %s. clusterHealth: %t", cluster.Name, clusterHealth)
		}
		if cluster.Name == "nothealth" && clusterHealth == true {
			t.Errorf("nothealth cluster should ignore.clustername: %s. clusterHealth: %t", cluster.Name, clusterHealth)
		}
		if cluster.Name == "health" && clusterHealth == false {
			t.Errorf("health cluster should not ignore.clustername: %s. clusterHealth: %t", cluster.Name, clusterHealth)
		}
	}
}
