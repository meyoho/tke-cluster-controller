/*
Copyright 2019 The Alauda Infrastructure Team.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cluster

import (
	"context"
	"fmt"
	"reflect"
	"time"

	clusterregistryv1alpha1 "bitbucket.org/tke-cluster-controller/pkg/apis/clusterregistry/v1alpha1"
	platformv1 "bitbucket.org/tke-cluster-controller/pkg/apis/platform/v1"
	resource "bitbucket.org/tke-cluster-controller/pkg/resource"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

var log = logf.Log.WithName("controller")
var healthCheckType = "HealthCheck"
var timeInterval = time.Duration(15)

/**
* USER ACTION REQUIRED: This is a scaffold file intended for the user to modify with their own Controller
* business logic.  Delete these comments after modifying this file.*
 */

// Add creates a new Cluster Controller and adds it to the Manager with default RBAC. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileCluster{Client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("cluster-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to Cluster
	err = c.Watch(&source.Kind{Type: &platformv1.Cluster{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &clusterregistryv1alpha1.Cluster{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &platformv1.Cluster{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &corev1.Secret{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &platformv1.Cluster{},
	})
	if err != nil {
		return err
	}

	err = c.Watch(&source.Kind{Type: &platformv1.ClusterCredential{}}, &handler.EnqueueRequestForOwner{
		IsController: true,
		OwnerType:    &platformv1.Cluster{},
	})
	if err != nil {
		return err
	}

	return nil
}

var _ reconcile.Reconciler = &ReconcileCluster{}

// ReconcileCluster reconciles a Cluster object
type ReconcileCluster struct {
	client.Client
	scheme *runtime.Scheme
}

// Reconcile reads that state of the cluster for a Cluster object and makes changes based on the state read
// and what is in the Cluster.Spec
// TODO(user): Modify this Reconcile function to implement your Controller logic.  The scaffolding writes
// a Deployment as an example
// Automatically generate RBAC rules to allow the Controller to read and write Deployments
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=platform.tke.cloud.tencent.com,resources=clusters,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=platform.tke.cloud.tencent.com,resources=clusters/status,verbs=get;update;patch
func (r *ReconcileCluster) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	log.Info("Get event.", "name: ", request.NamespacedName.Name)
	// Fetch the Cluster instance
	instance := &platformv1.Cluster{}
	err := r.Get(context.TODO(), types.NamespacedName{Name: request.NamespacedName.Name}, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			log.Error(err, "Can't find instance:", "name: ", request.NamespacedName, "detail: ", err.Error())
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, nil
		}
		log.Error(err, "Get instance failed.", "instance: ", request.NamespacedName, "detail: ", err.Error())
		// Error reading the object - requeue the request.
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}
	// If cluster is in unhealth state or initial state or unknow state, ignore it.
	if !isClusterHealth(instance) {
		log.Info("Cluster is not health. skip", "cluster: ", request.NamespacedName)
		return reconcile.Result{}, nil
	}
	// Assemble the seret resource
	credential, err := r.getCredential(instance.Name)
	if err != nil {
		log.Error(err, "get credential fail.", "name: ", instance.Name)
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}
	secret, err := resource.AssembleSecretResource(&credential)
	if err != nil {
		log.Error(err, "get token from tke cluster fail.", "name: ", instance.Name, "detail: ", err.Error())
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}

	// Assemble the cluster registry cluster resource
	cluster, err := resource.AssembleClusterRegistryResource(instance)
	if err != nil {
		log.Error(err, "get cluster registry resource from tke cluster fail.", "name: ", instance.Name, "detail: ", err.Error())
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}

	// Set the reference to sectet
	if err := controllerutil.SetControllerReference(instance, secret, r.scheme); err != nil {
		log.Error(err, "set reference to secret fail.", "secret-name: ", secret.Name, "detail: ", err.Error())
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}

	// Set the referene to cluster
	if err := controllerutil.SetControllerReference(instance, cluster, r.scheme); err != nil {
		log.Error(err, "set reference to cluster fail.", "cluster-name: ", cluster.Name, "detail: ", err.Error())
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}

	changed := false
	// Try to get the secret. If not exist, create it.
	secretFound := &corev1.Secret{}
	err = r.Get(context.TODO(), types.NamespacedName{Name: secret.Name, Namespace: secret.Namespace}, secretFound)
	if err != nil && errors.IsNotFound(err) {
		log.Info("can't find secret, create it.", "secret-name: ", secretFound.Name)
		err = r.Create(context.TODO(), secret)
		if err != nil {
			log.Error(err, "create secret fail.", "secret-name", secretFound.Name, "detail: ", err.Error())
			return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
		}
	} else if err != nil {
		log.Error(err, "get secret fail.", "secret-name: ", secretFound.Name, "detail: ", err.Error())
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	} else {
		changed = true
	}

	// Try to get the cluster registry resource. If not exist, create it.
	clusterFound := &clusterregistryv1alpha1.Cluster{}
	err = r.Get(context.TODO(), types.NamespacedName{Name: cluster.Name, Namespace: cluster.Namespace}, clusterFound)
	if err != nil && errors.IsNotFound(err) {
		log.Info("can't find cluster, create it.", "cluster-name", cluster.Name)
		err = r.Create(context.TODO(), cluster)
		if err != nil {
			log.Error(err, "create cluster fail.", "cluster-name", cluster.Name, "detail: ", err.Error())
			return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
		}
	} else if err != nil {
		log.Error(err, "get cluster fail.", "cluster-name: ", cluster.Name, "detail: ", err.Error())
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	} else {
		changed = true
	}

	remoteClient, err := resource.CreateK8sClient(cluster.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress, secret.Data[resource.TokenKey])
	if err != nil {
		log.Error(err, "Create k8s client fail.", "cluster-name: ", instance.Name)
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}

	cpu, memory := resource.GetCpuAndMemory(instance)
	equal, err := resource.IsEqual(remoteClient, cpu, memory)
	if err != nil {
		log.Error(err, "isEqual fail.", "cluster-name: ", instance.Name)
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}
	if equal == false {
		log.Info("ratio changed.", "cluster-name: ", instance.Name)
		changed = true
	}

	if changed == false {
		log.Info("Nothing changed.", "cluster-name: ", instance.Name)
		return reconcile.Result{}, nil
	}

	// Compare the exist secret to the current secret. If not equal, update it.
	if !reflect.DeepEqual(secret.Data, secretFound.Data) {
		log.Info("secret is different, update it", "secret-name: ", secret.Name)
		secretFound.Data = secret.Data
		if err := r.Update(context.TODO(), secretFound); err != nil {
			log.Error(err, "update secret fail.", "secret-name: ", secretFound.Name, "detail: ", err.Error())
			return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
		}
	}

	// Compare the exist cluster resource to the current one. If not equal, update it.
	if !reflect.DeepEqual(cluster.Spec, clusterFound.Spec) {
		log.Info("registry cluster is different, update it", "cluster-name: ", cluster.Name)
		log.Info("updating cluster...", "cluster-name: ", cluster.Name)
		clusterFound.Spec = cluster.Spec
		if err := r.Update(context.TODO(), clusterFound); err != nil {
			log.Error(err, "update cluster fail.", "cluster-name: ", clusterFound.Name, "detail: ", err.Error())
			return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
		}
	}

	err = resource.CreateOrUpdateRatioInstance(remoteClient, cpu, memory)
	if err != nil {
		log.Error(err, "set ratio fail.", "cluster-name: ", instance.Name)
		return reconcile.Result{Requeue: true, RequeueAfter: time.Second * timeInterval}, err
	}
	log.Info("Success sync cluster.", "cluster-name: ", instance.Name)
	return reconcile.Result{}, nil
}

func isClusterHealth(tkeCluster *platformv1.Cluster) bool {
	for i := 0; i < len(tkeCluster.Status.Conditions); i++ {
		if tkeCluster.Status.Conditions[i].Type == healthCheckType {
			if tkeCluster.Status.Conditions[i].Status == platformv1.ConditionTrue {
				return true
			} else {
				log.Info("skip unhealthy cluster.", "name: ", tkeCluster.Name, "current-status: ", tkeCluster.Status.Conditions[i].Status)
				return false
			}
		}
	}
	log.Info("can't find cluster healthcheck status. Ignore this event.", "cluster-name: ", tkeCluster.Name)
	return false
}

// TODO add test
func (r *ReconcileCluster) getCredential(name string) (platformv1.ClusterCredential, error) {
	credentials := &platformv1.ClusterCredentialList{}
	listOption := &client.ListOptions{}
	// Because controller runtime time client can't filter resource with fieldSelector. So we give up fieldSelector way, use for loop way. https://github.com/kubernetes-sigs/kubebuilder/issues/547
	// listOption.SetFieldSelector(fmt.Sprintf("clusterName=%s", name))

	// err := r.List(context.TODO(), listOption, credentials)
	// if err != nil {
	// 	log.Error(err, "get credential from tke fail.", "name: ", name, "listOption: ", listOption.FieldSelector)
	// 	return nil, err
	// }
	// if len(credentials.Items) != 1 {
	// 	log.Error(err, "fail to get credential for cluster", "cluster_name: ", name, "credentials number: ", len(credentials.Items))
	// 	return nil, err
	// }
	// return &credentials.Items[0], nil
	err := r.List(context.TODO(), listOption, credentials)
	if err != nil {
		log.Error(err, "get credential from tke fail.", "name: ", name, "listOption: ", listOption.FieldSelector)
		return platformv1.ClusterCredential{}, nil
	}
	for i := 0; i < len(credentials.Items); i++ {
		if credentials.Items[i].ClusterName == name {
			return credentials.Items[i], nil
		}
	}
	return platformv1.ClusterCredential{}, fmt.Errorf("can not get credential with clusterName: %s", name)
}
