package resource

import (
	"encoding/json"
	"reflect"
	"testing"

	clusterregistryv1alpha1 "bitbucket.org/tke-cluster-controller/pkg/apis/clusterregistry/v1alpha1"
	platformv1 "bitbucket.org/tke-cluster-controller/pkg/apis/platform/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestGetClientCIDR(t *testing.T) {
	cidr := "192.168.0.0/16"
	clusters := []*platformv1.Cluster{
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "withcidr"},
			Spec: platformv1.ClusterSpec{
				ClusterCIDR: "192.168.0.0/16",
			},
		},
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "withoutcidr"},
		},
	}
	t.Run("withcidr", func(t *testing.T) {
		clusterCIDR, _ := getClientCIDR(clusters[0])
		if cidr != clusterCIDR {
			t.Errorf("Get cluster cidr fail")
		}
	})

	t.Run("withoutcidr", func(t *testing.T) {
		clusterCIDR, err := getClientCIDR(clusters[1])
		if clusterCIDR != "" || err == nil {
			t.Errorf("Get cluster without cidr should fail")
		}
	})
}

func TestGetServiceAddress(t *testing.T) {
	apiserver := "https://114.114.114.114:8443"
	clusters := []*platformv1.Cluster{
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "noaddress"},
		},
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "advertiseip"},
			Status: platformv1.ClusterStatus{
				Addresses: []platformv1.ClusterAddress{
					platformv1.ClusterAddress{
						Type: platformv1.AddressAdvertise,
						Host: "10.96.1.1",
						Port: int32(123),
					},
					platformv1.ClusterAddress{
						Type: platformv1.AddressInternal,
						Host: "127.0.0.1",
						Port: int32(234),
					},
				},
			},
		},
		&platformv1.Cluster{
			ObjectMeta: metav1.ObjectMeta{Name: "haspubliip"},
			Status: platformv1.ClusterStatus{
				Addresses: []platformv1.ClusterAddress{
					platformv1.ClusterAddress{
						Type: platformv1.AddressInternal,
						Host: "127.0.0.1",
						Port: int32(234),
					},
					platformv1.ClusterAddress{
						Type: platformv1.AddressPublic,
						Host: "114.114.114.114",
						Port: int32(8443),
					},
				},
			},
		},
	}
	t.Run("noaddress", func(t *testing.T) {
		url, err := getServiceAddress(clusters[0])
		if err == nil || len(url) > 0 {
			t.Errorf("noaddress should return error")
		}
	})
	t.Run("advertiseip", func(t *testing.T) {
		url, err := getServiceAddress(clusters[1])
		if err != nil {
			t.Errorf("advertiseip should success. err: %s", err.Error())
		} else if url != "https://10.96.1.1:123" {
			t.Errorf("advertiseip should success. url: %s", url)
		}
	})
	t.Run("haspublicip", func(t *testing.T) {
		url, err := getServiceAddress(clusters[2])
		if err != nil {
			t.Errorf("has public ip should success. err: %s", err.Error())
		} else if url != apiserver {
			t.Errorf("has public ip should success. url: %s", url)
		}
	})
}

func TestGetToken(t *testing.T) {
	token := "aaabbbcccddd"

	credentials := []*platformv1.ClusterCredential{
		&platformv1.ClusterCredential{
			ObjectMeta: metav1.ObjectMeta{Name: "withouttoken"},
			CACert:     []byte("aabbb"),
		},
		&platformv1.ClusterCredential{
			ObjectMeta: metav1.ObjectMeta{Name: "withtoken"},
			Token:      &token,
		},
	}

	t.Run("withouttoken", func(t *testing.T) {
		token, err := getToken(credentials[0])
		if token != "" || err == nil {
			t.Errorf("withouttoken test should return error")
		}
	})
	t.Run("withtoken", func(t *testing.T) {
		token, err := getToken(credentials[1])
		if len(token) == 0 || err != nil {
			t.Errorf("withtoken should return right token")
		}
	})
}

func TestAssembleSecretResource(t *testing.T) {
	token := "YWFhYmJiYgo="
	credential := &platformv1.ClusterCredential{
		ObjectMeta:  metav1.ObjectMeta{Name: "notrealyname"},
		ClusterName: "realclustername",
		Token:       &token,
	}
	secret := corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "tke-cluster-realclustername",
			Namespace: "alauda-system",
		},
		Data: map[string][]byte{
			TokenKey: []byte(token),
		},
	}
	assembledSecret, err := AssembleSecretResource(credential)
	if err != nil {
		t.Errorf("assemble secret from tke cluster fail")
	}
	if !reflect.DeepEqual(secret, *assembledSecret) {
		t.Errorf("2 secret should deep equal")
	}
}

func TestAssembleClusterRegistryResource(t *testing.T) {
	tkeCluster := &platformv1.Cluster{
		ObjectMeta: metav1.ObjectMeta{Name: "clusterregistry"},
		Spec: platformv1.ClusterSpec{
			ClusterCIDR: "192.168.0.0/16",
			DisplayName: "this-is-displayname",
		},
		Status: platformv1.ClusterStatus{
			Addresses: []platformv1.ClusterAddress{
				platformv1.ClusterAddress{
					Type: platformv1.AddressPublic,
					Host: "10.96.1.1",
					Port: int32(123),
				},
				platformv1.ClusterAddress{
					Type: platformv1.AddressInternal,
					Host: "127.0.0.1",
					Port: int32(234),
				},
				platformv1.ClusterAddress{
					Type: platformv1.AddressAdvertise,
					Host: "114.114.114.114",
					Port: int32(8443),
				},
			},
		},
	}
	clusterregistryCluster := &clusterregistryv1alpha1.Cluster{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "clusterregistry",
			Namespace: "alauda-system",
			Annotations: map[string]string{
				"alauda.io/display-name": "this-is-displayname",
			},
		},
		Spec: clusterregistryv1alpha1.ClusterSpec{
			AuthInfo: clusterregistryv1alpha1.AuthInfo{
				Controller: &clusterregistryv1alpha1.ObjectReference{
					Kind:      "Secret",
					Name:      "tke-cluster-clusterregistry",
					Namespace: "alauda-system",
				},
			},
			KubernetesAPIEndpoints: clusterregistryv1alpha1.KubernetesAPIEndpoints{
				ServerEndpoints: []clusterregistryv1alpha1.ServerAddressByClientCIDR{
					clusterregistryv1alpha1.ServerAddressByClientCIDR{
						ClientCIDR:    "192.168.0.0/16",
						ServerAddress: "https://114.114.114.114:8443",
					},
				},
			},
		},
	}
	assembleCluster, err := AssembleClusterRegistryResource(tkeCluster)
	if err != nil {
		t.Errorf("assemble cluster from tke cluster fail")
	}
	if !reflect.DeepEqual(clusterregistryCluster, assembleCluster) {
		clusterByte, err := json.Marshal(clusterregistryCluster)
		if err != nil {
			t.Errorf("marshal cluster fail. %s", err.Error())
		}
		assembleClusterByte, err := json.Marshal(assembleCluster)
		if err != nil {
			t.Errorf("marshal assemble cluster fail. %s", err.Error())
		}
		t.Errorf("2 cluster resource shoule deep equal.\ncluster:\n%s\nassemblecluster:\n%s\n", string(clusterByte), string(assembleClusterByte))
	}
}
