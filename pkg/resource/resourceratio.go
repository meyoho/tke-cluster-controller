package resource

import (
	"context"
	"fmt"
	"reflect"
	"strconv"
	"time"

	kerrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes/scheme"
	restclient "k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"

	featurev1alpha1 "bitbucket.org/tke-cluster-controller/pkg/apis/feature/v1alpha1"
	platformv1 "bitbucket.org/tke-cluster-controller/pkg/apis/platform/v1"
)

var ratioResourceName = "resourceratio"
var ratioNamespacedName = types.NamespacedName{Name: ratioResourceName}

func GetCpuAndMemory(tkeCluster *platformv1.Cluster) (int, int) {
	clusterProperty := tkeCluster.Spec.Properties
	oversoldRatioValue := reflect.ValueOf(clusterProperty.OversoldRatio)
	oversoldRatio := oversoldRatioValue.Interface().(map[string]string)
	cpu, memory := int(1), int(1)
	if oversoldRatio == nil {
		return 1, 1
	} else {
		for k, v := range oversoldRatio {
			if k == "cpu" {
				c, e := strconv.Atoi(v)
				if e == nil {
					cpu = c
				}
			} else if k == "memory" {
				m, e := strconv.Atoi(v)
				if e == nil {
					memory = m
				}
			}
		}
	}
	return cpu, memory
}

func IsEqual(client client.Client, cpu, memory int) (bool, error) {
	ratioInstance := &featurev1alpha1.Feature{}
	err := client.Get(context.TODO(), ratioNamespacedName, ratioInstance)
	if err != nil {
		log.Info("get ratio instance fail. But ignore this error, just mark not equal", "error: ", err.Error())
		return false, nil
		// if kerrors.IsNotFound(err) {
		// 	return false, nil
		// } else {
		// 	return false, fmt.Errorf("Get resourceratio fail. detail: %s", err.Error())
		// }
	}
	if ratioInstance.Spec.DeployInfo.Cpu != cpu || ratioInstance.Spec.DeployInfo.Memory != memory {
		return false, nil
	}
	return true, nil
}

func CreateOrUpdateRatioInstance(client client.Client, cpu, memory int) error {
	ratioInstance := &featurev1alpha1.Feature{}
	err := client.Get(context.TODO(), ratioNamespacedName, ratioInstance)
	if err != nil {
		if kerrors.IsNotFound(err) {
			ratioInstance.Name = ratioResourceName
			ratioInstance.Spec.Version = "1.0"
			ratioInstance.Spec.Type = ratioResourceName
			ratioInstance.Spec.DeployInfo = featurev1alpha1.DeployInfo{
				Cpu:    cpu,
				Memory: memory,
			}
			log.Info("ratioInstance not exist. create it.", "cpu: ", cpu, "memory: ", memory)
			createErr := client.Create(context.TODO(), ratioInstance)
			if createErr != nil {
				return fmt.Errorf("create resourceratio fail. error info: %s", createErr.Error())
			}
		} else {
			return fmt.Errorf("Get resourceratio fail detail: %s", err.Error())
		}
	}
	changed := false
	if ratioInstance.Spec.DeployInfo.Cpu != cpu {
		changed = true
		ratioInstance.Spec.DeployInfo.Cpu = cpu
	}
	if ratioInstance.Spec.DeployInfo.Memory != memory {
		changed = true
		ratioInstance.Spec.DeployInfo.Memory = memory
	}
	if changed == true {
		log.Info("ratio changed, update it.", "cpu: ", cpu, "memory: ", memory)
		if err := client.Update(context.TODO(), ratioInstance); err != nil {
			return fmt.Errorf("Update resourceratio fail. detail: %s", err.Error())
		}
	}

	return nil
}

func CreateK8sClient(apiserver string, token []byte) (client.Client, error) {
	cfg := &restclient.Config{
		Host:        apiserver,
		BearerToken: string(token),
		TLSClientConfig: restclient.TLSClientConfig{
			Insecure: true,
		},
		Timeout: time.Second * 2,
	}
	c, err := client.New(cfg, client.Options{Scheme: scheme.Scheme})
	if err != nil {
		log.Error(err, "create k8s client fail.")
		return nil, err
	}
	return c, nil
}
