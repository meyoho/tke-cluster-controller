package resource

import (
	"errors"
	"flag"
	"fmt"
	"reflect"

	clusterregistryv1alpha1 "bitbucket.org/tke-cluster-controller/pkg/apis/clusterregistry/v1alpha1"
	platformv1 "bitbucket.org/tke-cluster-controller/pkg/apis/platform/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var log = logf.Log.WithName("formater")
var SystemNamespace string
var clusterSecretPrefix string
var TokenKey string
var labelBaseDomain string

func init() {
	TokenKey = "token"
	flag.StringVar(&SystemNamespace, "system-namespace", "alauda-system", "The namespace of system resources are stored")
	flag.StringVar(&labelBaseDomain, "label-basedomain", "alauda.io", "The base domain of all default labels")
	flag.StringVar(&clusterSecretPrefix, "cluster-secret-prefix", "tke-cluster-", "The prefix of the secret name which store the token of the cluster")
}

func getClientCIDR(tkeCluster *platformv1.Cluster) (string, error) {
	clusterCIDRValue := reflect.ValueOf(tkeCluster.Spec.ClusterCIDR)
	clusterCIDR := clusterCIDRValue.Interface().(string)
	if clusterCIDR == "" && tkeCluster.Spec.Type != platformv1.ClusterImported {
		return "", errors.New("get cidr fail. Can't find ClusterCidr info in Spec")
	}
	return tkeCluster.Spec.ClusterCIDR, nil
}

func getServiceAddress(tkeCluster *platformv1.Cluster) (string, error) {
	clusterAddressesValue := reflect.ValueOf(tkeCluster.Status.Addresses)
	clusterAddresses := clusterAddressesValue.Interface().([]platformv1.ClusterAddress)
	advertiseIp, publicIp, realIp, internalIp, supportIP := "", "", "", "", ""
	for i := 0; i < len(clusterAddresses); i++ {
		address := fmt.Sprintf("https://%s:%d", tkeCluster.Status.Addresses[i].Host, tkeCluster.Status.Addresses[i].Port)
		switch tkeCluster.Status.Addresses[i].Type {
		case platformv1.AddressAdvertise:
			advertiseIp = address
		case platformv1.AddressPublic:
			publicIp = address
		case platformv1.AddressReal:
			realIp = address
		case platformv1.AddressSupport:
			supportIP = address
		case platformv1.AddressInternal:
			internalIp = address
		}
	}
	if len(realIp) > 0 {
		return realIp, nil
	} else if len(advertiseIp) > 0 {
		return advertiseIp, nil
	} else if len(publicIp) > 0 {
		return publicIp, nil
	} else if len(supportIP) > 0 {
		return supportIP, nil
	} else if len(internalIp) > 0 {
		return internalIp, nil
	} else {
		return "", errors.New("get service address from tke cluster failed. None address found.")
	}
}

func getToken(tkeCredential *platformv1.ClusterCredential) (string, error) {
	tokenValue := reflect.ValueOf(tkeCredential.Token)
	token := tokenValue.Interface().(*string)
	if token == nil {
		return "", errors.New("get token fail. CAnt find Token info in Credential")
	}
	if len(*tkeCredential.Token) > 0 {
		return *tkeCredential.Token, nil
	}
	return "", errors.New("get token but it is empty.")
}

func AssembleSecretResource(tkeCredential *platformv1.ClusterCredential) (*corev1.Secret, error) {
	token, err := getToken(tkeCredential)
	if err != nil {
		return nil, err
	}
	return &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      clusterSecretPrefix + tkeCredential.ClusterName,
			Namespace: SystemNamespace,
		},
		Data: map[string][]byte{
			TokenKey: []byte(token),
		},
	}, nil
}

func AssembleClusterRegistryResource(tkeCluster *platformv1.Cluster) (*clusterregistryv1alpha1.Cluster, error) {
	clientCIDR, err := getClientCIDR(tkeCluster)
	if err != nil {
		log.Error(err, "get clientCIDR fail.", "name: ", tkeCluster.Name)
		return nil, err
	}

	serviceAddress, err := getServiceAddress(tkeCluster)
	if err != nil {
		log.Error(err, "get serviceAddress fail.", "name: ", tkeCluster.Name)
		return nil, err
	}

	displayName := fmt.Sprintf("%s/display-name", labelBaseDomain)
	cluster := &clusterregistryv1alpha1.Cluster{
		ObjectMeta: metav1.ObjectMeta{
			Name:      tkeCluster.Name,
			Namespace: SystemNamespace,
			Annotations: map[string]string{
				displayName: tkeCluster.Spec.DisplayName,
			},
		},
		Spec: clusterregistryv1alpha1.ClusterSpec{
			AuthInfo: clusterregistryv1alpha1.AuthInfo{
				Controller: &clusterregistryv1alpha1.ObjectReference{
					Kind:      "Secret",
					Name:      clusterSecretPrefix + tkeCluster.Name,
					Namespace: SystemNamespace,
				},
			},
			KubernetesAPIEndpoints: clusterregistryv1alpha1.KubernetesAPIEndpoints{
				ServerEndpoints: []clusterregistryv1alpha1.ServerAddressByClientCIDR{
					clusterregistryv1alpha1.ServerAddressByClientCIDR{
						ClientCIDR:    clientCIDR,
						ServerAddress: serviceAddress,
					},
				},
			},
		},
	}
	return cluster, nil
}
