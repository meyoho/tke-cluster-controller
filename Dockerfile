# Build the manager binary
FROM index.alauda.cn/alaudaorg/alaudabase-alpine-go:1.12-alpine3.10 as builder

# Copy in the go src
WORKDIR /go/src/bitbucket.org/tke-cluster-controller
COPY pkg/    pkg/
COPY cmd/    cmd/
COPY vendor/ vendor/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o manager bitbucket.org/tke-cluster-controller/cmd/manager

# Copy the controller-manager into a thin image
FROM index.alauda.cn/alaudaorg/alaudabase-alpine-run:alpine3.10
WORKDIR /
COPY --from=builder /go/src/bitbucket.org/tke-cluster-controller/manager .
ENTRYPOINT ["/manager"]
