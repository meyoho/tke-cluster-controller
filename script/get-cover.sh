#!/bin/bash
result=$(go tool cover -func=cover.out | tail -n 1 | awk 'BEGIN{FS=" "} {print $3}' | sed -e "s/%$//")
sed -i "" "s/coverage-.*%25/coverage-${result}%25/g" ./README.md
