
# Image URL to use all building/pushing image targets
IMG ?= controller:latest

all: test manager cover

# Run tests
test: generate fmt vet manifests
#	go test ./pkg/... ./cmd/... -coverprofile cover.out
	go test ./pkg/controller/cluster/...  ./pkg/resource/...  -coverprofile cover.out

cover:
	./script/get-cover.sh

# Test detail
testdetail:
	go tool cover -func=cover.out
# Test html
testhtml:
	go tool cover -html=cover.out

# Build manager binary
manager: generate fmt vet
	go build -o bin/manager bitbucket.org/tke-cluster-controller/cmd/manager

# Run against the configured Kubernetes cluster in ~/.kube/config
run: generate fmt vet
	go run ./cmd/manager/main.go

# Install CRDs into a cluster
install: manifests
	kubectl apply -f config/crds

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: manifests
	kubectl apply -f config/crds
	kustomize build config/default | kubectl apply -f -

# Generate manifests e.g. CRD, RBAC etc.
manifests:
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go crd --apis-path=pkg/apis/clusterregistry --domain=k8s.io
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go crd --apis-path=pkg/apis/platform --domain=tke.cloud.tencent.com
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go crd --apis-path=pkg/apis/feature --domain=infrastructure.alauda.io
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go rbac
	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go webhook
#	go run vendor/sigs.k8s.io/controller-tools/cmd/controller-gen/main.go all

# Run go fmt against code
fmt:
	go fmt ./pkg/... ./cmd/...

# Run go vet against code
vet:
	go vet ./pkg/... ./cmd/...

# Generate code
generate:
ifndef GOPATH
	$(error GOPATH not defined, please define GOPATH. Run "go help gopath" to learn more about GOPATH)
endif
	go generate ./pkg/... ./cmd/...

# Build the docker image
docker-build: test
	docker build . -t ${IMG}
	@echo "updating kustomize image patch file for manager resource"
	sed -i'' -e 's@image: .*@image: '"${IMG}"'@' ./config/default/manager_image_patch.yaml

# Push the docker image
docker-push:
	docker push ${IMG}
